confluent相关的依赖，需要在在maven的settings.xml中增加如下配置
    <repositories>
        <repository>
            <id>confluent</id>
            <url>http://packages.confluent.io/maven/</url>
        </repository>
    </repositories>
    <mirrors>
        <mirror>
            <id>confluent</id>
            <mirrorOf>confluent</mirrorOf>
            <name>Nexus public mirror</name>
            <url>http://packages.confluent.io/maven/</url>
        </mirror>
    </mirrors>