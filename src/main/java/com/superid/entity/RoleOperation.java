package com.superid.entity;

import org.apache.avro.generic.GenericRecord;

/**
 * 角色操作实体类，需要将此类的信息发送到kafka上
 * @author dufeng
 * @create: 2018-08-08 11:17
 */
public class RoleOperation extends AvroEntity {

    private long roleId;
    private long userId;
    private String operationLog;
    private int type;
    private int state;
    private long createTime;
    private long operatorUserId;
    private long operatorRoleId;

    private RoleOperation(){

    }

    private RoleOperation(Builder builder) {
        roleId = builder.roleId;
        userId = builder.userId;
        operationLog = builder.operationLog;
        type = builder.type;
        state = builder.state;
        createTime = builder.createTime;
        operatorUserId = builder.operatorUserId;
        operatorRoleId = builder.operatorRoleId;
    }


    public static final class Builder {
        private long roleId;
        private long userId;
        private String operationLog;
        private int type;
        private int state;
        private long createTime;
        private long operatorUserId;
        private long operatorRoleId;

        public Builder() {
        }

        public Builder roleId(long val) {
            roleId = val;
            return this;
        }

        public Builder userId(long val) {
            userId = val;
            return this;
        }

        public Builder operationLog(String val) {
            operationLog = val;
            return this;
        }

        public Builder type(int val) {
            type = val;
            return this;
        }

        public Builder state(int val) {
            state = val;
            return this;
        }

        public Builder createTime(long val) {
            createTime = val;
            return this;
        }

        public Builder operatorUserId(long val) {
            operatorUserId = val;
            return this;
        }

        public Builder operatorRoleId(long val) {
            operatorRoleId = val;
            return this;
        }

        public RoleOperation build() {
            return new RoleOperation(this);
        }
    }

    @Override
    public GenericRecord getAvroRecord() throws Exception {
        return super.getAvroRecord();
    }
}
