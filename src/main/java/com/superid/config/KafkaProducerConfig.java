package com.superid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author dufeng
 * @create: 2018-08-09 09:15
 */
@Configuration
@ConfigurationProperties(prefix = "spring.kafka.producer")
public class KafkaProducerConfig {

    private int retries = Integer.MAX_VALUE;

    private int bufferMemory =33554432;

    private boolean enableIdempotence = true;

    private int maxInFlightRequestsPerConnection = 1;

    private int batchSize = 16384;

    private int lingerMs =1;

    private String acks = "all";

    private String topic;

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    public int getBufferMemory() {
        return bufferMemory;
    }

    public void setBufferMemory(int bufferMemory) {
        this.bufferMemory = bufferMemory;
    }

    public boolean isEnableIdempotence() {
        return enableIdempotence;
    }

    public void setEnableIdempotence(boolean enableIdempotence) {
        this.enableIdempotence = enableIdempotence;
    }

    public int getMaxInFlightRequestsPerConnection() {
        return maxInFlightRequestsPerConnection;
    }

    public void setMaxInFlightRequestsPerConnection(int maxInFlightRequestsPerConnection) {
        this.maxInFlightRequestsPerConnection = maxInFlightRequestsPerConnection;
    }

    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    public int getLingerMs() {
        return lingerMs;
    }

    public void setLingerMs(int lingerMs) {
        this.lingerMs = lingerMs;
    }

    public String getAcks() {
        return acks;
    }

    public void setAcks(String acks) {
        this.acks = acks;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    @Override
    public String toString() {
        return "KafkaProducerConfig{" +
                "retries=" + retries +
                ", bufferMemory=" + bufferMemory +
                ", enableIdempotence=" + enableIdempotence +
                ", maxInFlightRequestsPerConnection=" + maxInFlightRequestsPerConnection +
                ", batchSize=" + batchSize +
                ", lingerMs=" + lingerMs +
                ", acks='" + acks + '\'' +
                ", topic='" + topic + '\'' +
                '}';
    }
}
