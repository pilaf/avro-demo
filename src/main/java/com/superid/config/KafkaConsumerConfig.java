package com.superid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author dufeng
 * @create: 2018-08-08 14:11
 */
@Configuration
@ConfigurationProperties(prefix = "spring.kafka.consumer")
public class KafkaConsumerConfig {

    private String autoOffsetReset ="earliest";

    private boolean enableAutoCommit = false;

    private String groupId;

    private List<String> topics;

    public String getAutoOffsetReset() {
        return autoOffsetReset;
    }

    public void setAutoOffsetReset(String autoOffsetReset) {
        this.autoOffsetReset = autoOffsetReset;
    }

    public boolean isEnableAutoCommit() {
        return enableAutoCommit;
    }

    public void setEnableAutoCommit(boolean enableAutoCommit) {
        this.enableAutoCommit = enableAutoCommit;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }

    @Override
    public String toString() {
        return "KafkaConsumerConfig{" +
                "autoOffsetReset='" + autoOffsetReset + '\'' +
                ", enableAutoCommit=" + enableAutoCommit +
                ", groupId='" + groupId + '\'' +
                ", topics=" + topics +
                '}';
    }
}
