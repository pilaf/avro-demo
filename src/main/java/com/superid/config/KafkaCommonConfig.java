package com.superid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Kafka生产者和消费者共同的配置
 * @author dufeng
 * @create: 2018-08-08 20:36
 */
@Configuration
@ConfigurationProperties(prefix = "spring.kafka")
public class KafkaCommonConfig {

    private List<String> bootstrapServers = new ArrayList<>();

    private List<String> schemaRegistryUrl = new ArrayList<>();

    public List<String> getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(List<String> bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public List<String> getSchemaRegistryUrl() {
        return schemaRegistryUrl;
    }

    public void setSchemaRegistryUrl(List<String> schemaRegistryUrl) {
        this.schemaRegistryUrl = schemaRegistryUrl;
    }


    @Override
    public String toString() {
        return "KafkaCommonConfig{" +
                "bootstrapServers=" + bootstrapServers +
                ", schemaRegistryUrl=" + schemaRegistryUrl +
                '}';
    }
}
