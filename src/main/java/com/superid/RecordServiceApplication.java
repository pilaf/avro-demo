package com.superid;

import com.superid.clients.AvroKafkaConsumer;
import com.superid.clients.AvroKafkaProducer;
import com.superid.config.KafkaConsumerConfig;
import com.superid.config.KafkaProducerConfig;
import com.superid.entity.RoleOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecordServiceApplication implements CommandLineRunner{

	@Autowired
    AvroKafkaProducer producer;

    @Autowired
    AvroKafkaConsumer consumer;

    @Autowired
    private KafkaProducerConfig producerConfig;
    @Autowired
    private KafkaConsumerConfig consumerConfig;

	public static void main(String[] args) {
		SpringApplication.run(RecordServiceApplication.class, args);

	}

	@Override
	public void run(String... args) throws Exception {
		for(int i=0;i<100;i++) {
			producer.send(producerConfig.getTopic(),new RoleOperation.Builder()
					.userId(i+200)
					.operationLog("好好学习，天天向上"+i)
					.state(0)
					.build().getAvroRecord());
		}


        consumer.consume(consumerConfig.getTopics());
    }
}
